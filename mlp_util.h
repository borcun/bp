#ifndef MLP_UTIL_H
#define MLP_UTIL_H

#include <cmath>
#include <cstdio>

#define TRAIN_DATA_FILE  ( ( const char * ) "asset/train_data.txt" )
#define TRAIN_CLASS_FILE ( ( const char * ) "asset/train_class.txt" )

// minimum error ratio
#define MLP_MIN_ERROR_RATIO ( 0.5 )

// weight count for Input Neuron
#define MLP_INPUT_NEURON_WEIGHT_COUNT  ( 50 )

// input data count for Hidden Neuron
#define MLP_HIDDEN_NEURON_INPUT_COUNT  ( 3 )
// weight count for Hidden Neuron
#define MLP_HIDDEN_NEURON_WEIGHT_COUNT ( 3 )

// input data count for Hidden Neuron
#define MLP_OUTPUT_NEURON_INPUT_COUNT  ( 50 )

// MLP design parameters which are designed customly
enum MLPDesignParameters {
  // total input neuron count 
  MLP_INPUT_NEURON_COUNT  = 3,

  // total hidden neuron count 
  MLP_HIDDEN_NEURON_COUNT = 50,
  
  // total output neuron count 
  MLP_OUTPUT_NEURON_COUNT = 3
};

// Input Neuron structure
struct MLPInputNeuron {
  double input;
  double delta;
  double weight[ MLP_INPUT_NEURON_WEIGHT_COUNT ];
  double ( *activeFn )( const double );
};

// Hidden Neuron structure
struct MLPHiddenNeuron {
  double input;
  double delta;
  double weight[ MLP_HIDDEN_NEURON_WEIGHT_COUNT ];
  double ( *activeFn )( const double );
  double ( *errFn )( const double, const double );
};

// Output Neuron structure
struct MLPOutputNeuron {
  double input;
  double delta;
  double ( *activeFn )( const double );
  double ( *errFn )( const double, const double );
};


// function that calculates sigmoid formula for x parameter
inline double sigmoid( const double x ) {
  return 1.0 / ( 1.0 + exp( -x ) );
}

// function that calculates hyperbolic tangent formula for x parameter
inline double hTan( const double x ) {
  return tanh( x );
}

inline double bpError( const double output, const double expected ) {
  return ( expected - output ) * ( output * ( 1.0 - output ) );
}

#endif
