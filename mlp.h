#ifndef MLP_H
#define MLP_H

#include "mlp_util.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <unistd.h>

// MLP class
class MLP {
 public:
  // default constructor
  MLP( void );

  // destructor
  virtual ~MLP();

  // function that reads train data and train class files
  // if initialization is OK, return true. Otherwise, return false
  bool init( void );

  // function that train network which runs backpropogation algorithm
  // param err_amount - error amount to stop process
  void train( const double err_amount );
  
 private:
  // input neurons
  MLPInputNeuron iNeuron[ MLP_INPUT_NEURON_COUNT ];

  // hidden neurons
  MLPHiddenNeuron hNeuron[ MLP_HIDDEN_NEURON_COUNT ];

  // output neurons
  MLPOutputNeuron oNeuron[ MLP_OUTPUT_NEURON_COUNT ];
  
  // function that manages forward operation which calculate
  // activation values from input layer to output layer
  void forward( void );

  // function that manages backward operation which trains weights
  void backward( void );

};

#endif
