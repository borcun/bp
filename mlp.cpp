#include "mlp.h"

MLP::MLP( void ) {
  srand( time( NULL ) );
}

MLP::~MLP() {

}

bool MLP::init( void ) {
  memset( iNeuron, 0x00, sizeof( MLPInputNeuron ) * MLP_INPUT_NEURON_COUNT );
  memset( hNeuron, 0x00, sizeof( MLPHiddenNeuron ) * MLP_HIDDEN_NEURON_COUNT );
  memset( oNeuron, 0x00, sizeof( MLPOutputNeuron ) * MLP_OUTPUT_NEURON_COUNT );
  
  for( int i=0 ; i < MLP_INPUT_NEURON_COUNT ; ++i ) {
    /// @todo read from file
    iNeuron[i].input = ( double ) ( rand() % 100 ) / 10.0;

    for( int j=0 ; j < MLP_INPUT_NEURON_WEIGHT_COUNT ; ++j ) {
      iNeuron[i].weight[j] = ( double ) ( rand() % 100 ) / 1000.0;
    }

    iNeuron[i].activeFn = sigmoid;
  }

  for( int i=0 ; i < MLP_HIDDEN_NEURON_COUNT ; ++i ) {
    for( int j=0 ; j < MLP_HIDDEN_NEURON_WEIGHT_COUNT ; ++j ) {
      hNeuron[i].weight[j] = ( double ) ( rand() % 100 ) / 1000.0;
    }

    hNeuron[i].activeFn = sigmoid;
    hNeuron[i].errFn = bpError;
  }
  
  for( int i=0 ; i < MLP_OUTPUT_NEURON_COUNT ; ++i ) {
    oNeuron[i].activeFn = sigmoid;
    oNeuron[i].errFn = bpError;
  }

  return true;
}

void MLP::train( const double err_amount ) {
  for( int i=0 ; i < 10 ; ++i ) {
    forward();
    backward();
    printf( "--------------------------------------\n" );
  }
  
  return;
}

void MLP::forward( void ) {
  // input neurons -> hidden neurons
  for( int i=0 ; i < MLP_HIDDEN_NEURON_COUNT ; ++i ) {
    hNeuron[ i ].input = 0.0;
    
    for( int j=0 ; j < MLP_INPUT_NEURON_COUNT ; ++j ) {
      hNeuron[ i ].input += ( iNeuron[ j ].input * iNeuron[ j ].weight[ i ] );
    }

    hNeuron[ i ].input = hNeuron[ i ].activeFn( hNeuron[ i ].input );    
  }

  // hidden neurons -> output neurons
  for( int i=0 ; i < MLP_OUTPUT_NEURON_COUNT ; ++i ) {
    oNeuron[ i ].input = 0.0;
    
    for( int j=0 ; j < MLP_HIDDEN_NEURON_COUNT ; ++j ) {
      oNeuron[ i ].input += ( hNeuron[ j ].input * hNeuron[ j ].weight[ i ] );
    }

    // calculate input and error amount of output neurons
    oNeuron[ i ].input = oNeuron[ i ].activeFn( oNeuron[ i ].input );
  }

  return;
}

void MLP::backward( void ) {
  for( int i=0 ; i < MLP_OUTPUT_NEURON_COUNT ; ++i ) {
    oNeuron[ i ].delta = oNeuron[ i ].errFn( oNeuron[ i ].input, 0.0 );
  }

  for( int i=0 ; i < MLP_HIDDEN_NEURON_COUNT ; ++i ) {
    hNeuron[i].delta = 0.0;
      
    for( int j=0 ; j < MLP_OUTPUT_NEURON_COUNT ; ++j ) {
      hNeuron[i].delta += ( hNeuron[i].weight[j] * oNeuron[j].delta );
    }
  }

  for( int i=0 ; i < MLP_INPUT_NEURON_COUNT ; ++i ) {
    iNeuron[i].delta = 0.0;
      
    for( int j=0 ; j < MLP_HIDDEN_NEURON_COUNT ; ++j ) {
      iNeuron[i].delta += ( iNeuron[i].weight[j] * hNeuron[j].delta );
    }

    for( int j=0 ; j < MLP_HIDDEN_NEURON_WEIGHT_COUNT ; ++j ) {
      printf( "%d.%d old weight %f, ", i, j, hNeuron[i].weight[j] );
      hNeuron[i].weight[j] = hNeuron[i].weight[j] * hNeuron[i].delta * hNeuron[i].input;
      printf( "current weight %f\n", hNeuron[i].weight[j] );
    }
  }

  for( int i=0 ; i < MLP_INPUT_NEURON_COUNT ; ++i ) {     
    for( int j=0 ; j < MLP_HIDDEN_NEURON_COUNT ; ++j ) {
      iNeuron[i].delta += ( iNeuron[i].weight[j] * hNeuron[j].delta );
    }

    for( int j=0 ; j < MLP_INPUT_NEURON_WEIGHT_COUNT ; ++j ) {
      iNeuron[i].weight[j] = iNeuron[i].weight[j] * iNeuron[i].delta * iNeuron[i].input;
    }
  }

  return;
}
