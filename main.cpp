#include "mlp.h"

int main( int argc, char **argv ) {
  MLP mlp;
  const double err_amount = 1.0;
  
  if( mlp.init() ) {
    mlp.train( err_amount );
  }

  printf( "Backpropogation is completed\n" );

  return 0;
}
