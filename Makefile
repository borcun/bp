CC=g++
CFLAGS=-c -Wall
DBGFLAG=-g
LFLAGS=-o
SRC=mlp.cpp main.cpp
OBJ=mlp.o main.o
EXEC=mlp
RM=rm -rf

all:
	${CC} ${CFLAGS} ${SRC}
	${CC} ${OBJ} ${LFLAGS} ${EXEC}

debug:
	${CC} ${CFLAGS} ${DBGFLAG} ${SRC}
	${CC} ${OBJ} ${LFLAGS} ${EXEC}

clean:
	${RM} ${OBJ} ${EXEC}
